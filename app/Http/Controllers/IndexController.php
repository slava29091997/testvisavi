<?php

namespace App\Http\Controllers;

use App\Firm;

use Illuminate\Http\Request;



use DB;

class IndexController extends Controller
{
    //
    public function execute(Request $request){

        $pages = Firm::all();

        $inquiry = DB::select("select * from Firms where id not in (select FirmID from Phones)");
        $inquiry2 = DB::select("select * , COUNT(*) from Firms f, Phones p where f.id = p.FirmID GROUP by p.FirmID HAVING COUNT(*) >= 2");
        $inquiry3 = DB::select("select * , COUNT(*) from Firms f, Phones p where f.id = p.FirmID GROUP by p.FirmID HAVING COUNT(*) < 2");
        $inquiry4 = DB::select("select * , COUNT(*) from Firms f, Phones p where f.id = p.FirmID GROUP by p.FirmID ORDER by COUNT(*) DESC LIMIT 1");


        return view('site.index',array(
            'pages'=>$pages,
            'inquiry'=>$inquiry,
            'inquiry2'=>$inquiry2,
            'inquiry3'=>$inquiry3,
            'inquiry4'=>$inquiry4
        ));
    }

}
