<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::all();    //Получаємо дані з бази даних


        return view('admin.home',array(  //Передаємо дані в Види
            'books'=>$book,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $date = Book::find($id);




        return view('site.articles',array(  //Передаємо дані в Види
            'date'=>$date,
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $date2 = $request;

        //$pages = $test = DB::update("UPDATE `Books` SET `name` = '$date2->name', `email` = '$date2->email', `text` = '$date2->text' WHERE `Books`.`id` = $id");

        if ($pages = $test = DB::update("UPDATE `Books` SET `name` = '$date2->name', `email` = '$date2->email', `text` = '$date2->text' WHERE `Books`.`id` = $id")){
            return redirect('admin')->with('status','Материал обновлен');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $flight = Book::find($id);


        if ($flight->delete()){
            return redirect('admin/articless')->with('status','Дані удалены');
        }
    }
}
