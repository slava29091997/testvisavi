<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firm extends Model
{
    public function phone(){
        return $this->hasOne('App\Phone','FirmID');
    }

}
