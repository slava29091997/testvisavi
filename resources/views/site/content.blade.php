<div style="margin:0px 50px 0px 50px;">

    @if($pages)
        <a class="navbar-brand" href="{{ url('article') }}">
            {{  'Гостевая книга' }}
        </a>
        <a class="navbar-brand" href="{{ url('admin') }}">
            {{  'Admin' }}
        </a>
        <table class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th class="text-center">Name</th>
                <th class="text-center">Phone</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $page)

                <tr>
                    <td>{{$page->name}}</td>
                    @if(is_object($page->phone))
                    <td>{{$page->phone->Phone}}</td>
                    @else
                        <td>--</td>
                    @endif
                </tr>

            @endforeach

            </tbody>
        </table>

    @endif

        @if($inquiry)
            <h5>Все фирмы, не имеющие телефонов : @foreach ($inquiry as $item)
                    {{$item->name}}
                @endforeach
            </h5>
        @endif
        @if($inquiry2)
            <h5>Все фирмы, имеющие не менее 2-х телефонов : @foreach ($inquiry2 as $item)
                    {{$item->name}}
                @endforeach
            </h5>
        @endif
        @if($inquiry3)
            <h5>Все фирмы, имеюшие менее 2-х телефонов : @foreach ($inquiry3 as $item)
                    {{$item->name}}
                @endforeach
            </h5>
        @endif
        @if($inquiry4)
            <h5>Фирма, имеющая максимальное кол-во телефонов : @foreach ($inquiry4 as $item)
                    {{$item->name}}
                @endforeach
            </h5>
        @endif



</div>
