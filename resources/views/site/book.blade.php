@extends('layouts.site')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <a class="navbar-brand" href="{{ url('/') }}">
        {{  'Головна' }}
    </a>
    <a class="navbar-brand" href="{{ url('admin') }}">
        {{  'Admin' }}
    </a>
    <br><br>
        <div class="container">
            <div class="row">
                <div class="row">
                        <div class="center-block wow fadeInLeft delay-02s">
                            <div class="form">
                                <form action="{{route('article.store')}}" method="post">
                                    <div class="detail">
                                        <h4>Name</h4>
                                    </div>
                                    <input class="input-text" type="text" name="name"  onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                                    <div class="detail">
                                        <h4>Email</h4>
                                    </div>
                                    <input class="input-text" type="text" name="email"  onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                                    <div class="detail">
                                        <h4>Text</h4>
                                    </div>
                                    <textarea name="text" class="input-text text-area" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;"></textarea>
                                    <input class="input-btn" type="submit" value="send message">

                                    {{ csrf_field() }}

                                </form>


                            </div>
                        </div>
                </div>
        </div>



            <section class="page_section contact" id="contact">

                <div >
                    <h2>Гостевая книга</h2>
                </div>


            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="center-block wow fadeInLeft delay-02s">

                                        @if ($books)
                                            @foreach($books as $k=>$book)
                                            <div class="detail">
                                                <h4>Имя : {{$book->name}} </h4>
                                            </div>
                                            <div class="detail">
                                                <h4>Email : {{$book->email}}</h4>
                                            </div>
                                            <div class="detail">
                                                <h4>Текст : {{$book->text}}</h4>
                                            </div>
                                                <div class="detail">
                                                    <h4>Дата створення : {{$book->created_at}}</h4>
                                                </div>
                                                <div class="borderTop"></div>
                                                <br>
                                        @endforeach
                                    @endif
                                     </div>

                                    <ul class="social_links">
                                        <li class="twitter animated bounceIn wow delay-02s"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                        <li class="facebook animated bounceIn wow delay-03s"><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                        <li class="pinterest animated bounceIn wow delay-04s"><a href="javascript:void(0)"><i class="fa fa-pinterest"></i></a></li>
                                        <li class="gplus animated bounceIn wow delay-05s"><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                </div>
            </div>
            </section>
@endsection


