@extends('layouts.site')


@section('content')

    <div class="container">
        <div class="row">
            <div class="row">
                <div class="center-block wow fadeInLeft delay-02s">
                    <div class="form">
                        <form action="{{route('articless.update',['articles'=>$date->id])}}" method="POST">
                            <div class="detail">
                                <h4>Name</h4>
                            </div>
                            <input class="input-text" type="text" name="name" value="{{$date->name}}" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                            <div class="detail">
                                <h4>Email</h4>
                            </div>
                            <input class="input-text" type="text" name="email" value="{{$date->email}}"  onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                            <div class="detail">
                                <h4>Text</h4>
                            </div>
                            <textarea name="text" class="input-text text-area" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">{{$date->text}}</textarea>

                            @if(isset($date->id))
                                <input type="hidden" name="_method" value="PUT">

                            @endif


                            <input class="input-btn" type="submit" value="send message">

                            {{ csrf_field() }}

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

