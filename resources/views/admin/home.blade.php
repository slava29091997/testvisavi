@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">



                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                        @if(isset($books))
                            <div id="content-page" class="content group">
                                <div class="hentry group">
                                    <div class="short-table white">
                                        <table style="width: 100%" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <th class="align-left">ID</th>
                                                <th>name</th>
                                                <th>email</th>
                                                <th>text</th>
                                                <th>created_at</th>
                                                <th>Дествие</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($books as $book)
                                                <tr>
                                                    <td class="align-left">{{$book->id}}</td>
                                                    <td class="align-left">{!! Html::link(route('articless.edit',['articles'=>$book->id]),$book->name) !!}</td>
                                                    <td class="align-left">{{$book->email}}</td>
                                                    <td class="align-left">{{$book->text}}</td>
                                                    <td class="align-left">{{$book->created_at}}</td>

                                                    <td>
                                                        {!! Form::open(['url' => route('articless.destroy',['articles'=>$book->id]),'class'=>'form-horizontal','method'=>'POST']) !!}
                                                        {{ method_field('DELETE') }}
                                                        {!! Form::button('Удалить', ['class' => 'btn btn-french-5','type'=>'submit']) !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>



                                </div>
                                <!-- START COMMENTS -->
                                <div id="comments">
                                </div>
                                <!-- END COMMENTS -->
                            </div>
                        @endif

        </div>
    </div>
</div>
@endsection
