<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Phones', function (Blueprint $table) {
            $table->integer('FirmID')->unsigned();

            $table->foreign('FirmID')->references('id')->on('Firms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Phones', function (Blueprint $table) {
            //
        });
    }
}
